# -*- coding: utf-8 -*-

"""
:mod: `sample_sim_movie` is a small demo script running a BioSim
       simulation that make use of the ability to make a movie out of images stored throughout
       the simulation given directory and file name. The movie will be saved as a "mpeg4" file by
       default.
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import textwrap
from biosim.simulation import BioSim

if __name__ == '__main__':

    geogr = """\
                   WWWWWWWWWWWWWWWWWWWWW
                   WHHHHDDDDDDHHHHHHHWWW
                   WWHHHHDDDDDDLLLLHHHWW
                   WWHHHHDDDLLLLLHHHWWWW
                   WHHHHHDDDDDLLLLLHHHWW
                   WHHHHDDDDDDLLLLLHHHWW
                   WWHHHHDDDDDLLLLLHWWWW
                   WWWHHHHLLLLLLLHHHWWWW
                   WWWHHHHHHDDDDDDWWWWWW
                   WWWWWWWWWWWWWWWWWWWWW"""
    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (7, 13),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(100)]}]

    ini_carns = [{'loc': (7, 14),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(25)]}]

    sim = BioSim(geogr, ini_herbs + ini_carns, seed=123,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 60.0, 'delta': 2},
                             'weight': {'max': 60, 'delta': 2}},
                 cmax_animals={'Herbivore': 200, 'Carnivore': 50},
                 img_dir='results',
                 img_base='sample')
    sim.simulate(100)
    sim.make_movie()

    input('Press ENTER')
