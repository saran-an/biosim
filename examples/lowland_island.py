# -*- coding: utf-8 -*-

"""

"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import matplotlib.pyplot as plt
from biosim.simulation import BioSim

if __name__ == '__main__':

    geogr = """\
               WWWWWWWWWWWWWWWWWWWWW
               WLLLLLLLLLLLLLLLLLLLW
               WLLLLLWWWDLDWWWLLLLLW
               WLLLLLWDDDLDDDWLLLLLW
               WLLLLLWDHHHHHDWLLLLLW
               WLLLLLWDHHHHHDWLLLLLW
               WWWWWWWWWWWWWWWWWWWWW
               WLLLLLWDHHHHHDWLLLLLW
               WLLLLLWDHHHHHDWLLLLLW
               WLLLLLWDDDLDDDWLLLLLW
               WLLLLLWWWDLDWWWLLLLLW
               WLLLLLLLLLLLLLLLLLLLW
               WWWWWWWWWWWWWWWWWWWWW"""

    ini_herbs1 = [{'loc': (5, 11),
                   'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20} for _ in range(100)]}]

    ini_herbs2 = [{'loc': (8, 11),
                   'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 20} for _ in range(100)]}]

    ini_carns1 = [{'loc': (5, 11),
                   'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 20} for _ in range(20)]}]

    ini_carns2 = [{'loc': (8, 11),
                   'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 20} for _ in range(20)]}]

    Rossomøya = BioSim(island_map=geogr, ini_pop=ini_herbs1 + ini_herbs2, seed=1,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 60.0, 'delta': 2},
                             'weight': {'max': 60, 'delta': 2}},
                 cmax_animals={'Herbivore': 200, 'Carnivore': 50}, ymax_animals=35000,
                 vis_years=1)

    Rossomøya.simulate(100)
    Rossomøya.add_population(population=ini_carns1+ini_carns2)
    Rossomøya.simulate(num_years=300)

    plt.savefig('lowland_island.pdf')
