# -*- coding: utf-8 -*-

"""
:mod: `sim_herbivores_desert` is a small demo script running a BioSim
       simulation that has a initial population of herbivores placed on an island
       of only desert landscape. The simulation will see the herbivores die out after a
       couple of years
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

from biosim.simulation import BioSim

if __name__ == "__main__":
    geogr = """\
                    WWWWWWWW
                    WDDDDDDW
                    WDDDDDDW
                    WDDDDDDW
                    WDDDDDDW
                    WDDDDDDW
                    WDDDDDDW
                    WWWWWWWW"""

    ini_herbs = [{"loc": (5, 5),
                  "pop": [{"species": "Herbivore", "age": 5, "weight": 50}
                          for _ in range(1000)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs, seed=123,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 60.0, 'delta': 2},
                             'weight': {'max': 60, 'delta': 2}},
                 vis_years=1)
    sim.simulate(30)

