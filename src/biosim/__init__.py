"""
Python package for BioSim, Modelling the Ecosystem of Rossumøya.
"""

__author__ = 'Pradeep Manoraj, Saranjan Anpalagan'
__email__ = 'pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no'
__version__ = '1.0'
