# -*- coding: utf-8 -*-

"""
This script has an island class, which provides the annual cycle of
the Rossumøya every year. The functions are created give a map of the Island.

It is required that the following python enviroment is installed to run this script:
"textwrap" and "numpy".
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import numpy as np
from biosim.landscape import Water, Lowland, Highland, Desert


class Island:
    """
    This is the base cLass for the island, Rossumøya.
    """

    island_landscapes = {"H": Highland, "L": Lowland, "D": Desert, "W": Water}

    def __init__(self, island_map, ini_pop=None):
        """
        Constructor that initiates an instance of class Island.

        Parameters
        ----------
        island_map: str
                Multiline string indicating geography of the island
        ini_pop: list
                List of dictionaries takes initial locations and the populations
                for the respective locations.
        """

        self.island_lines = island_map.splitlines()
        self.island_map = {}
        self.initial_pop = ini_pop

        row_length = len(self.island_lines[0])
        for lines in self.island_lines:
            if len(lines) is not row_length:
                raise ValueError("The string in each row must have the same length")

            for cell_type in lines:
                if cell_type not in self.island_landscapes.keys():
                    raise ValueError(f"Cell type {cell_type} does not exist")

            if lines[0] != "W" or lines[-1] != "W":
                raise ValueError("Island must be surrounded by water, to keep the island boundary."
                                 "Make sure every line start and ends with 'W'")

        for index in range(row_length):
            if self.island_lines[0][index] != "W" or self.island_lines[-1][index] != "W":
                raise ValueError("Island must be surrounded by water, to keep the island boundary."
                                 "Make sure the first and last line consists only by 'W' ")

        self.convert_to_locations()
        self.add_population()

    def convert_to_locations(self):
        """
        Converting the geography into coordinates for the Island and locations.
        """
        for x_pos, lines in enumerate(self.island_lines):
            for y_pos, cell_type in enumerate(lines):
                x_loc = 1 + x_pos
                y_loc = 1 + y_pos
                self.island_map[(x_loc, y_loc)] = self.island_landscapes[cell_type]()

    def add_population(self, additional_pop=None):
        """
        Give the possibility to add the animals to the Island map
        by using location and species type from ini_list

        Parameters
        ----------
        additional_pop: list
            List of dictionaries with specifications of locations and population
            for the respective locations.
        """
        if additional_pop is None:
            population = self.initial_pop
        else:
            population = additional_pop

        for location_animals in population:
            loc = location_animals["loc"]

            if loc not in self.island_map.keys():
                raise ValueError(f"The location {loc} is not valid in this Island.")
            if self.island_map[loc].habitable is False:
                raise ValueError(f"Location {loc} is {type(self.island_map[loc]).__name__}, "
                                 f" and not habitable.")

            pop = location_animals["pop"]
            self.island_map[loc].add_population(pop)

    @staticmethod
    def emigrating_location(location):
        """
        Coordinates of the location for emigration.

        Parameters
        ----------
        location: tuple
            Coordinates of current location

        Returns
        -------
        next_location: tuple
            Coordinates of next possible location
        """
        x_loc, y_loc = location
        neighbouring_coordinates = [(x_loc - 1, y_loc), (x_loc + 1, y_loc),
                                    (x_loc, y_loc - 1), (x_loc, y_loc + 1)]

        random_direction = np.random.choice(len(neighbouring_coordinates))
        next_location = neighbouring_coordinates[random_direction]

        return next_location

    def migration(self, location):
        """
        Move the animals that can emigrate from location to one of four immediately adjacent
        neighbouring locations.
        Parameters
        ----------
        location: tuple
            Current location with population

        """
        current_cell = self.island_map[location]
        if current_cell.habitable:
            emigrating_herbivores, emigrating_carnivores = current_cell.emigration()

            for herbivore in emigrating_herbivores:
                next_location = self.emigrating_location(location)
                next_cell = self.island_map[next_location]
                if next_cell.habitable:
                    next_cell.add_immigrated_animal(herbivore)
                    current_cell.herbivores.remove(herbivore)

            for carnivore in emigrating_carnivores:
                next_location = self.emigrating_location(location)
                next_cell = self.island_map[next_location]
                if next_cell.habitable:
                    next_cell.add_immigrated_animal(carnivore)
                    current_cell.carnivores.remove(carnivore)

    def reset_migration(self):
        """
        Make all the animals in island to reset their moved-status to no(False),
        so they can move in next cycle.
        """
        for location in self.island_map:
            cell = self.island_map[location]
            if cell.habitable:
                cell.reset_migration()

    def annual_cycle(self):
        """
        Simulate the annual cycle of island for all habitable cells.
        """
        for location in self.island_map:
            cell = self.island_map[location]
            if cell.habitable:
                cell.regrowth_fodder()
                cell.herbivores_eat()
                cell.carnivores_eat()
                cell.procreation()
                self.migration(location)
                cell.aging()
                cell.weight_loss()
                cell.death()
        self.reset_migration()

    def get_num_pr_species(self):
        """
        Total number of Herbivores and Carnivores in island.

        Returns
        -------
        num_animals: dict
            Dictionary with total number of herbivores and carnivores in island.
        """
        num_herbivores = 0
        num_carnivores = 0

        for location in self.island_map:
            cell = self.island_map[location]
            if cell.habitable:
                num_herbivores += cell.get_num_herbivores()
                num_carnivores += cell.get_num_carnivores()

        num_animals = {'Herbivore': num_herbivores, 'Carnivore': num_carnivores}
        return num_animals

    def get_num_population(self):
        """
        Total number of animals in island.

        Returns
        -------
        population: int
            Number of animals in island.
        """
        num_animals = self.get_num_pr_species()
        num_population = num_animals['Herbivore'] + num_animals['Carnivore']
        return num_population

    @property
    def species_attributes(self):
        """
        Attributes of every single animal in island.
        Attributes contains information about age, weight and fitness.
        Returns
        -------
        herbivores_attributes: dict
            Dictionary containing age, weight and fitness information for all herbivores in island.
        carnivores_attributes: dict
            Dictionary containing age, weight and fitness information for all herbivores in island.
        """
        herbivores_attributes = {'age': [], 'weight': [], 'fitness': []}
        carnivores_attributes = {'age': [], 'weight': [], 'fitness': []}

        herbivore_age, herbivore_weight, herbivore_fitness = [], [], []
        carnivore_age,  carnivore_weight, carnivore_fitness = [], [], []

        for location in self.island_map:
            cell = self.island_map[location]
            if cell.habitable:
                for herbivore in cell.herbivores:
                    herbivore_age.append(herbivore.age)
                    herbivore_weight.append(herbivore.weight)
                    herbivore_fitness.append(herbivore.fitness)
                for carnivore in cell.carnivores:
                    carnivore_age.append(carnivore.age)
                    carnivore_weight.append(carnivore.weight)
                    carnivore_fitness.append(carnivore.fitness)

        herbivores_attributes['age'] = np.array(herbivore_age)
        herbivores_attributes['weight'] = np.array(herbivore_weight)
        herbivores_attributes['fitness'] = np.array(herbivore_fitness)

        carnivores_attributes['age'] = np.array(carnivore_age)
        carnivores_attributes['weight'] = np.array(carnivore_weight)
        carnivores_attributes['fitness'] = np.array(carnivore_fitness)

        return herbivores_attributes, carnivores_attributes
