# -*- coding: utf-8 -*-

"""
This module contains the necessary functions, methods and arguments for the module to provide a
graphic window of the island, Russomøya. The module consists of a main class "Visualization"
that creates a number of functions such as for to create a graphical window with associated
subplots. These subplots deal with the distribution of the various the animals on the island,
the number of animals of the various as well as the visualization of the geographical landscape on
the island with different color codes. In addition, it consists of two functions that provide the
ability to save and create files and movies.

Inspired by Hans Plesser Ekkehard, 2022. Lecture:

* ../randvis/graphics.py,
* ../randvis/simulation.py,
* ../plotting/time_counter.py
* ../plotting/Mapping.py

**Notice**

In the python environment, the following packages must be installed to run the module:
"numpy", "matplotlib.pyplot".
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import matplotlib.pyplot as plt
import subprocess
import numpy as np
import math
import os

# Update these variables to point to your ffmpeg and convert binaries
# If you installed ffmpeg using conda or installed both softwares in
# standard ways on your computer, no changes should be required.
_FFMPEG_BINARY = 'ffmpeg'
_MAGICK_BINARY = 'magick'

# update this to the directory and file-name beginning
# for the graphics files
_DEFAULT_GRAPHICS_NAME = 'dv'
_DEFAULT_IMG_FORMAT = 'png'
_DEFAULT_MOVIE_FORMAT = 'mp4'  # alternatives: mp4, gif


class Visualization:
    """
    This is the base class for visulization.
    """

    def __init__(self, cmax, hist_specs, img_years, img_dir, img_base, img_fmt):
        """
        Initiates for the Visualization class

        Parameters
        ----------
        cmax: dict
            Sets the max value of number of animals in the heat map distribution
        hist_specs: dict
            Dictionary with one entry per property for which a histogram shall be shown.
        img_years:
            years between visualizations saved to files (default: vis_years)
        img_dir: str
            String with path to directory for figures
        img_base: str
            String with beginning of file name for figures
        img_fmt: str
            String with file type for figures, e.g. 'png'

        """

        self.cmax = cmax
        self.hist_specs = hist_specs
        # For single graphic plot
        self._figure = None
        # Sets visibility of grid inside the figure to on or off
        self._grids = None
        # Island map
        self._island_map = None
        # The count of each animal every year
        self._animal_count = None
        # Herbivore distribution
        self._herb_dist = None
        # Carnivore distribution
        self._carn_dist = None
        # Histogram for age
        self._age_hist = None
        # Histogram for weight
        self._weight_hist = None
        # Histogram for fitness
        self._fitness_hist = None
        # Total Herbivore graph
        self._herb_line = None
        # Total Carnivore graph
        self._carn_line = None

        # Update the age histogram
        self._age_histogram = None
        self._weight_histogram = None
        self._fitness_histogram = None

        self._year_text = None
        self._year_count = None

        # Update the heatmap for herbivore and carnivore distribution
        self._herb_axis = None
        self._carn_axis = None

        if img_base is None:
            img_base = _DEFAULT_GRAPHICS_NAME

        self.img_dir = img_dir
        if img_dir is not None:
            self.img_path = os.path.join(img_dir, img_base)
        else:
            self.img_path = None

        self._img_fmt = img_fmt if img_fmt is not None else _DEFAULT_IMG_FORMAT

        self.img_years = img_years
        self.img_fmt = img_fmt
        self._img_ctr = 0

    def graph_window(self, x_lim, y_lim, year):
        """
        Create one graphic window to show the visualization of the simulation.

        Parameters
        ----------
        x_lim: int
            The upper limit for graphics, x-axis
        y_lim: int
            The upper limit for graphics, y-axis
        year: int
            The start year of the simulation graphics

        """
        if self._figure is None:
            self._figure = plt.figure(figsize=(12, 8))
            self._grids = self._figure.add_gridspec(5, 15)
            self._figure.tight_layout()
            plt.axis("off")

        """
        Create a subplot for map
        """
        if self._island_map is None:
            self._island_map = self._figure.add_subplot(self._grids[:2, :5])
            self._island_map.title.set_text("Island")

        """
        Create a subplot for age histogram
        """
        if self._age_hist is None:
            self._age_hist = self._figure.add_subplot(self._grids[4:, 5:10])
            self._age_hist.set_title("Age")

        """
        Create a subplot for weight histogram
        """
        if self._weight_hist is None:
            self._weight_hist = self._figure.add_subplot(self._grids[4:, 11:])
            self._weight_hist.set_title("Weight")

        """
        Create a subplot for fitness histogram
        """
        if self._fitness_hist is None:
            self._fitness_hist = self._figure.add_subplot(self._grids[4:, :4])
            self._fitness_hist.set_title("Fitness")

        """
        Create a subplot for animal count, where it count each animal every year
        """
        if self._animal_count is None:
            self._animal_count = self._figure.add_subplot(self._grids[:2, 9:])
            # Set length of x and y-axis
            self._animal_count.set_ylim(0, y_lim)
            # Set title for the  plot
            self._animal_count.title.set_text("Animal count")
        self._animal_count.set_xlim(0, x_lim+1)

        """
        Initiate total Herbivore graph
        """
        if self._herb_line is None:
            herb_plot = self._animal_count.plot(np.arange(0, x_lim+1), np.full(x_lim+1, np.nan),
                                                label="Herbivore", color="blue", marker='.')
            self._herb_line = herb_plot[0]
        else:
            x_data, y_data = self._herb_line.get_data()
            x_new = np.arange(x_data[-1] + 1, x_lim+1)
            if len(x_new) > 0:
                y_new = np.full(x_new.shape, np.nan)
                self._herb_line.set_data(np.hstack((x_data, x_new)), np.hstack((y_data, y_new)))

        """
        Initiate total Carnivore graph
        """
        if self._carn_line is None:
            carn_plot = self._animal_count.plot(np.arange(0, x_lim+1), np.full(x_lim+1, np.nan),
                                                label="Carnivore", color="red", marker='.')
            self._carn_line = carn_plot[0]
            self._animal_count.legend(loc="upper right")
        elif self._carn_line is not None:
            x_data, y_data = self._carn_line.get_data()
            x_new = np.arange(x_data[-1] + 1, x_lim+1)
            if len(x_new) > 0:
                y_new = np.full(x_new.shape, np.nan)
                self._carn_line.set_data(np.hstack((x_data, x_new)), np.hstack((y_data, y_new)))

        """
        Create a subplot for the herbivore distribution, heatmap
        """
        if self._herb_dist is None:
            self._herb_dist = self._figure.add_subplot(self._grids[2:4, :6])
            self._herb_dist.title.set_text("Herbivore distribution")

        """
        Create a subplot for the carnivore distribution, heatmap
        """
        if self._carn_dist is None:
            self._carn_dist = self._figure.add_subplot(self._grids[2:4, 9:])
            self._carn_dist.title.set_text("Carnivore distribution")

        """
        Create a subplot for year count
        """
        if self._year_count is None:
            self._year_count = self._figure.add_subplot(self._grids[:1, 6:8])
            # Specify, after how many years the graphics are to be updated.
            template = f"Year: {year}"
            self._year_text = self._year_count.text(
                0.5, 0.5,
                template.format(0),
                horizontalalignment="center",
                verticalalignment="center",
                transform=self._year_count.transAxes,
                fontsize=14,
            )
        self._year_count.axis("off")

    def map_graph(self, geography):
        """
        Makes a visualization of the given Island. Given as a legend, this function
        has color code for the different types of landscapes.

        Parameters
        ----------
        geography: str
            Multi-line string indicating geography of the island
        """

        #                   R    G    B
        rgb_value = {'W': (0.0, 0.0, 1.0),  # blue
                     'L': (0.0, 0.6, 0.0),  # dark green
                     'H': (0.5, 1.0, 0.5),  # light green
                     'D': (1.0, 1.0, 0.5)}  # light yellow

        map_rgb = [[rgb_value[column] for column in row]
                   for row in geography.splitlines()]

        self._island_map.imshow(map_rgb)
        ax_lg = self._figure.add_axes([0.38, 0.7, 0.05, 0.2])  # llx, lly, w, h
        ax_lg.axis("off")
        for ix, name in enumerate(('Water', 'Lowland',
                                   'Highland', 'Desert')):
            ax_lg.add_patch(plt.Rectangle((0., ix * 0.2), 0.3, 0.1,
                                          edgecolor='none',
                                          facecolor=rgb_value[name[0]]))
            ax_lg.text(0.35, ix * 0.2, name, transform=ax_lg.transAxes)

    def _update_year(self, island_year):
        """
        Update the year count for each year

        Parameters
        ----------
        island_year: int
            The current year of the simulation

        """
        self._year_text.set_text(f"Year: {island_year}")

    def _update_age(self, herbivores_attributes, carnivores_attributes):
        """
        Update the age histogram of the species on the Island.

        Parameters
        ----------
        herbivores_attributes: dict
            A dictionary with the attributes of Herbivores
        carnivores_attributes: dict
            A dictionary with the attributes of Carnivores

        """
        if self._age_histogram is None:
            herbs_age = herbivores_attributes["age"]
            carns_age = carnivores_attributes["age"]
            n = math.ceil((self.hist_specs["age"]["max"]) / self.hist_specs["age"]["delta"])
            max_range = self.hist_specs["age"]["max"]
            self._age_hist.clear()
            self._age_hist.set_title("Age")
            self._age_hist.hist(herbs_age, bins=int(n), range=(0, max_range),
                                histtype="step", color="blue")
            self._age_hist.hist(carns_age, bins=int(n), range=(0, max_range),
                                histtype="step", color="red")

    def _update_weight(self, herbivores_attributes, carnivores_attributes):
        """
        Update the weight histogram of the species.

        Parameters
        ----------
        herbivores_attributes: dict
            A dictionary with the attributes of Herbivores
        carnivores_attributes: dict
            A dictionary with the attributes of Carnivores

        """
        if self._weight_histogram is None:
            herbs_weight = herbivores_attributes["weight"]
            carns_weight = carnivores_attributes["weight"]
            n = math.ceil((self.hist_specs["weight"]["max"]) / self.hist_specs["weight"]["delta"])
            max_range = self.hist_specs["weight"]["max"]
            self._weight_hist.clear()
            self._weight_hist.set_title("Weight")
            self._weight_hist.hist(herbs_weight, bins=int(n), range=(0, max_range),
                                   histtype="step", color="blue")
            self._weight_hist.hist(carns_weight, bins=int(n), range=(0, max_range),
                                   histtype="step", color="red")

    def _update_fitness(self, herbivores_attributes, carnivores_attributes):
        """
        Update the fitness histogram of the species.

        Parameters
        ----------
        herbivores_attributes: dict
            A dictionary about the attributes of Herbivores
        carnivores_attributes: dict
            A dictionary about the attributes of Carnivores

        """
        if self._fitness_histogram is None:
            herbs_fitness = herbivores_attributes["fitness"]
            carns_fitness = carnivores_attributes["fitness"]
            n = math.ceil((self.hist_specs["fitness"]["max"]) / self.hist_specs["fitness"]["delta"])
            max_range = self.hist_specs["fitness"]["max"]
            self._fitness_hist.clear()
            self._fitness_hist.set_title("Fitness")
            self._fitness_hist.hist(herbs_fitness, bins=int(n), range=(0, max_range),
                                    histtype="step", color="blue")
            self._fitness_hist.hist(carns_fitness, bins=int(n), range=(0, max_range),
                                    histtype="step", color="red")

    def _update_total_animal(self, year, num_animals):
        """
        Updates the total animal on the island.

        Parameters
        ----------
        year: int
            The current year of the simulation of the island
        num_animals: dict
            A dictionary with total number of the animals in current year on the Island.

        """
        num_herbs = num_animals["Herbivore"]
        herbivore_y = self._herb_line.get_ydata()
        herbivore_y[year] = num_herbs
        self._herb_line.set_ydata(herbivore_y)

        num_carns = num_animals["Carnivore"]
        carnivore_y = self._carn_line.get_ydata()
        carnivore_y[year] = num_carns
        self._carn_line.set_ydata(carnivore_y)

    def _update_heatmap_herbivore(self, distribution):
        """
        Update how many herbivores that is present in each cell of the island every year.

        Parameters
        ----------
        distribution: DataFrame
            Contains the information of how many herbivores that is in each cell of the island
        """
        if self._herb_axis is not None:
            self._herb_axis.set_data(distribution.pivot("Row", "Col", "Herbivore"))
        else:
            self._herb_axis = self._herb_dist.imshow(
                distribution.pivot("Row", "Col", "Herbivore"),
                interpolation="nearest",
                vmin=0,
                vmax=self.cmax["Herbivore"],
            )
            self._herb_dist.figure.colorbar(self._herb_axis, ax=self._herb_dist,
                                            orientation="vertical")

    def _update_heatmap_carnivore(self, distribution):
        """
        Update how many Carnivores that is present in each cell of the island every year.

        Parameters
        ----------
        distribution: DataFrame
            Contains the information of how many Carnivores that is in each cell of the island
        """
        if self._carn_axis is not None:
            self._carn_axis.set_data(distribution.pivot("Row", "Col", "Carnivore"))
        else:
            self._carn_axis = self._carn_dist.imshow(
                distribution.pivot("Row", "Col", "Carnivore"),
                interpolation="nearest",
                vmin=0,
                vmax=self.cmax["Carnivore"],
            )
            self._carn_dist.figure.colorbar(self._carn_axis, ax=self._carn_dist,
                                            orientation="vertical")

    def _save_graphics(self, present_year):
        """
        Saves graphics to file if file name given. Saves the files in directory.

        Parameters
        ----------
        present_year: int
            Current year of the simulation
        """

        if self.img_path is None or present_year % self.img_years != 0:
            return

        try:
            os.makedirs(self.img_dir)
        except FileExistsError:
            pass

        plt.savefig('{base}_{num:05d}.{type}'.format(base=self.img_path,
                                                     num=self._img_ctr,
                                                     type=self.img_fmt))
        self._img_ctr += 1

    def make_movie(self, movie_fmt=None):
        """
        Creates MPEG4 movie from visualization images saved.

        .. :note:
            Requires ffmpeg for MP4 and magick for GIF

        The movie is stored as img_base + movie_fmt

        """

        if self.img_path is None:
            raise RuntimeError("No filename defined.")

        if movie_fmt is None:
            movie_fmt = _DEFAULT_MOVIE_FORMAT

        if movie_fmt == 'mp4':
            try:
                # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
                # section "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY,
                                       '-i', '{}_%05d.png'.format(self.img_path),
                                       '-y',
                                       '-profile:v', 'baseline',
                                       '-level', '3.0',
                                       '-pix_fmt', 'yuv420p',
                                       '{}.{}'.format(self.img_path, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))
        elif movie_fmt == 'gif':
            try:
                subprocess.check_call([_MAGICK_BINARY,
                                       '-delay', '1',
                                       '-loop', '0',
                                       '{}_*.png'.format(self.img_path),
                                       '{}.{}'.format(self.img_path, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: convert failed with: {}'.format(err))
        else:
            raise ValueError('Unknown movie format: ' + movie_fmt)

    def update_graph(self, year, distribution, num_animals, herbs_attributes, carns_attributes):
        """
        Update the graphs for every year that passes in the simulation

        Parameters
        ----------
        year: int
            Current year of the simulation
        distribution: df
            DataFrame with the information about the distribution of the different spices
        num_animals: dict
             A dictionary with total number of species in current year on the Island.
        herbs_attributes: dict
            A dictionary about the attributes of Herbivores
        carns_attributes: dict
            A dictionary about the attributes of Carnivores
        """
        self._update_heatmap_herbivore(distribution)
        self._update_heatmap_carnivore(distribution)
        self._update_year(year)
        self._update_age(herbs_attributes, carns_attributes)
        self._update_weight(herbs_attributes, carns_attributes)
        self._update_fitness(herbs_attributes, carns_attributes)
        self._update_total_animal(year, num_animals)
        self._figure.canvas.flush_events()
        self._save_graphics(year)
        plt.pause(1e-6)
