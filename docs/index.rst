.. BioSim documentation master file, created by
   sphinx-quickstart on Sat Jan 15 11:44:31 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Group A48's BioSim documentation!
============================================
A documentation of modelling the Ecosystem of an Island called Rossumøya.

.. image:: images/logo.png
   :width: 250px
   :height: 250px
   :align: center

Preface
---------
Aas, January 2022

This project assignment has been prepared in connection with a course called INF200
taught by Hans Ekkehard Plesser at the Norwegian University of Life Sciences, NMBU.
The course is about assessing the appropriateness of advanced programs and
can ensure the quality of these. Quality assurance is done by performing custom
tests using such as built-in packages. In addition, the subject learns to look at the development
and gain an insight into what responsibilities the programmer has. This is done by making the
correct and reliable decision of your own programs, quality assurance, and documentation.

Modules
---------
.. toctree::
   :maxdepth: 2

   animals
   landscape
   visualization
   simulation

Summary
---------
Rossumøya is a small island in the middle of vast ocean that belongs to
the island nation of Pylandia. The main purpose is to preserve the ecosystem
on the Island for future generations. The Island, Rossumøya, is characterized by landscapes and two
species. The landscapes are Lowland (L), Highland (H), Water (W) and Desert (D). The species are
Herbivores (Plant eaters) and Carnivores (predators).

With the following information, a simulation and visualization of the investigation of both species
can survive in the long term has been done.

.. image:: images/simulation.png
   :width: 500px
   :height: 300px
   :align: center

References
----------
Plesser, H. E (2022). Modelling the Ecosystem of Rossumøya. PDF:
https://gitlab.com/nmbu.no/emner/inf200/h2021/inf200-course-materials/-/blob/main/january_block/INF200_H21_BioSimJan_v2.pdf

Plesser, H. E (2022). BioSim Template. Lecture:
https://gitlab.com/nmbu.no/emner/inf200/h2021/inf200-course-materials/-/tree/main/january_block/biosim_template

Authors
-------
**Group A48: Pradeep Manoraj, Saranjan Anpalagan**

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
