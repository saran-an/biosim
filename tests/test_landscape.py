# -*- coding: utf-8 -*-

"""
Test set for Landscape class.

This set of tests checks of the Landscape class to be provided by
the simulation module of the biosim package.
"""

__author__ = "Pradeep Manoraj, Saranjan Anpalagan"
__email__ = "pradeep.manoraj@nmbu.no, saranjan.anpalagan@nmbu.no"

import pytest
import numpy as np
import itertools
from scipy.stats import binom_test

from biosim.landscape import Landscape, Lowland, Highland, Desert, Water
from biosim.animals import Herbivore, Carnivore

# alpha for limit of acceptance for statistical tests
ALPHA = 0.05


@pytest.fixture(autouse=True)
def reset_animal_default_parameters():
    """Sets the parameters for species to the default values in tests"""
    yield
    herbivore_standard_parameters = {
            'w_birth': 8.0,
            'sigma_birth': 1.5,
            'beta': 0.9,
            'eta': 0.05,
            'a_half': 40.0,
            'phi_age': 0.6,
            'w_half': 10.0,
            'phi_weight': 0.1,
            'mu': 0.25,
            'gamma': 0.2,
            'zeta': 3.5,
            'xi': 1.2,
            'omega': 0.4,
            'F': 10.0,
        }
    carnivore_standard_parameters = {
            "w_birth": 6.0,
            "sigma_birth": 1.0,
            "beta": 0.75,
            "eta": 0.125,
            "a_half": 40.0,
            "phi_age": 0.3,
            "w_half": 4.0,
            "phi_weight": 0.4,
            "mu": 0.4,
            "gamma": 0.8,
            "zeta": 3.5,
            "xi": 1.1,
            "omega": 0.8,
            "F": 50.0,
            "DeltaPhiMax": 10.0
        }
    Herbivore.set_params(herbivore_standard_parameters)
    Carnivore.set_params(carnivore_standard_parameters)


@pytest.fixture(autouse=True)
def reset_landscape_default_parameters():
    yield
    Lowland.set_params({'f_max': 800})
    Highland.set_params({'f_max': 300})


# TESTS FOR LOWLAND
@pytest.mark.parametrize('landscape_type', [Lowland, Highland])
def test_parameter_set(landscape_type):
    """Check the function to update the parameter values with other values"""

    landscape_type = landscape_type()
    pres_param = landscape_type.get_params()
    param = {'f_max': 1000.0}
    new_param = landscape_type.set_params(param)
    assert new_param is not pres_param


@pytest.mark.parametrize('landscape_types,', [Lowland, Highland, Desert, Water])
def test_initiation_landscape_type(landscape_types):
    """Check that lowland would be initialised properly"""
    instance = landscape_types()
    assert isinstance(instance, landscape_types)


@pytest.mark.parametrize('landscape_type', [Lowland, Highland])
def test_set_params_key_error(landscape_type):
    """Check if a invalid parameter-name is set, ValueError is displayed."""
    new_param = {"max_fodder": 12.0}

    with pytest.raises(KeyError):
        landscape_type.set_params(new_param)


@pytest.mark.parametrize('landscape_type', [Lowland, Highland])
def test_param_value_error(landscape_type):
    """Check if a invalid value raises ValueError for parameter f_max."""
    new_param_f_max = {"f_max": -10.0}

    with pytest.raises(ValueError):
        landscape_type.set_params(new_param_f_max)


@pytest.mark.parametrize('landscape_type', [Desert, Water])
def test_param_invalid_landscape_type(landscape_type):
    new_param_f_max = {"f_max": 110.0}

    with pytest.raises(ValueError):
        landscape_type.set_params(new_param_f_max)


@pytest.mark.parametrize('landscape_type, habitable_status', [(Lowland, True),
                                                              (Highland, True),
                                                              (Desert, True),
                                                              (Water, False)])
def test_landscape_habitable(landscape_type, habitable_status):
    """Tests that landscape is habitable and water is not"""
    assert landscape_type.habitable is habitable_status


@pytest.mark.parametrize('species, num_animals', [('Herbivore', 5), ('Carnivore', 6)])
def test_landscape_add_population(species, num_animals):
    """
    Tests that number of herbivore inserted in location is the same as population in location after
    instance os created.
    Also checks if the instances are initialised according species-argument in passed dictionaries.
    """
    ini_pop = [{'species': species, 'age': 5, 'weight': 20} for _ in range(num_animals)]
    landscape = Landscape()
    landscape.add_population(animals=ini_pop)
    assert landscape.get_num_pr_species()[species] == num_animals


def test_add_invalid_species():
    """Tests that adding a species not specified as valid, raises KeyError"""
    ini_bears = [{'species': 'Bear', 'age': 5, 'weight': 20} for _ in range(5)]
    landscape = Landscape()
    with pytest.raises(KeyError):
        landscape.add_population(animals=ini_bears)


@pytest.mark.parametrize('landscape_type', [Lowland, Highland])
def test_regrowth_fodder(landscape_type):
    """Tests that the fodder grows to the f-max amount after regrowth in Lowland"""
    landscape = landscape_type()
    landscape.f_available = 0
    landscape.regrowth_fodder()
    assert landscape.f_available == landscape_type.params['f_max']


class TestHerbivoreCarnivoreEat:
    """
    Tests for herbivore og carnivore eating.
    """

    @pytest.fixture(autouse=True)
    def create_lowland_add_herbivores_carnivores(self):
        """Create a lowland landscape and add herbivores"""
        self.num_herbs = 10
        self.num_carns = 2
        self.age, self.weight = 4, 20
        self.ini_herbs = [{'species': 'Herbivore', 'age': self.age, 'weight': self.weight}
                          for _ in range(self.num_herbs)]
        self.ini_carns = [{'species': 'Carnivore', 'age': self.age, 'weight': self.weight}
                          for _ in range(self.num_carns)]
        self.lowland = Lowland()
        self.lowland.add_population(animals=self.ini_herbs)
        self.lowland.add_population(animals=self.ini_carns)

    def test_herbivore_dont_eat(self):
        """Tests that herbivore dont eat if no fodder available"""
        self.lowland.f_available = 0
        weights_before = self.lowland.get_weight_pr_species()['Herbivore']
        self.lowland.herbivores_eat()
        weights_after = self.lowland.get_weight_pr_species()['Herbivore']
        assert (np.array(weights_after) == np.array(weights_before)).all()

    def test_herbivore_eat_only_when_f_available(self):
        """Tests that the herbivore eats maximum F_max when there is still more fodder available,
         and gains expected weight by beta*F_max"""
        herbivore_appetite = Herbivore.params['F']
        self.lowland.f_available = herbivore_appetite
        h1_weight_before = self.lowland.herbivores[0].weight
        h2_weight_before = self.lowland.herbivores[1].weight
        self.lowland.herbivores_eat()
        h1_weight_after = self.lowland.herbivores[0].weight
        h2_weight_after = self.lowland.herbivores[1].weight
        assert self.lowland.f_available == 0
        assert h1_weight_after == h1_weight_before + Herbivore.params['beta'] * herbivore_appetite
        assert h2_weight_after == h2_weight_before

    def test_herbivore_gain_weight_F_tilde(self):
        """Tests that herbivore eats only the amount fodder available,
         and the ones after dont eat when no fodder available."""
        herbivore_appetite = Herbivore.params['F']
        self.lowland.f_available = herbivore_appetite - 1
        F_tilde = self.lowland.f_available
        weight_before = self.lowland.herbivores[0].weight
        self.lowland.herbivores_eat()
        weight_after = self.lowland.herbivores[0].weight
        assert weight_after == (weight_before + Herbivore.params['beta'] * F_tilde)

    def test_herbivore_more_fitness_eat_first(self):
        """Test that herbivores with more fitness eat before those with less"""
        years = 2
        herbivore_appetite = Herbivore.params['F']
        herbi_1 = self.lowland.herbivores[0]
        herbi_2 = self.lowland.herbivores[1]
        herbi_3 = self.lowland.herbivores[2]
        for year in range(years):
            self.lowland.f_available = 2 * herbivore_appetite - 1
            self.lowland.herbivores_eat()
        assert herbi_1.weight > herbi_2.weight > herbi_3.weight
        assert herbi_1.fitness > herbi_2.fitness > herbi_3.fitness

    def test_herbivores_decreases_when_carnivore_eat(self, mocker):
        """Tests that herbivore number decreases when carnivores are hunting successfully"""
        mocker.patch('random.random', return_value=0)

        pop_herb_before = self.lowland.get_num_herbivores()
        self.lowland.carnivores_eat()
        pop_herb_after = self.lowland.get_num_herbivores()

        assert pop_herb_after < pop_herb_before

    def test_carnivore_eat_max_F_amount(self, mocker):
        """Tests that carnivore eat only the appetite amount F when hunting successfully and eating,
        even though the total weight of herbivore hunted and kill is more"""
        mocker.patch('random.random', return_value=0)

        carnivore_appetite = Carnivore.params['F']
        total_weight_herbivores_before = sum(self.lowland.get_weight_pr_species()['Herbivore'])
        self.lowland.carnivores_eat()
        total_weight_herbivores_after = sum(self.lowland.get_weight_pr_species()['Herbivore'])
        dead_herbivores_weight = total_weight_herbivores_before - total_weight_herbivores_after

        assert dead_herbivores_weight >= self.num_carns * carnivore_appetite


@pytest.mark.parametrize('species', ['Herbivore', 'Carnivore'])
def test_procreation_num_animal_one(species):
    """Test that population doesn't expand when there is only one animal when procreation"""
    ini_pop = [{'species': species, 'age': 5, 'weight': 20}]
    lowland = Lowland()
    lowland.add_population(ini_pop)
    num_before = lowland.get_num_pr_species()[species]
    lowland.procreation()
    num_after = lowland.get_num_pr_species()[species]
    assert num_after == num_before


@pytest.mark.parametrize('species, num_animals', [('Herbivore', 5), ('Carnivore', 6)])
def test_procreation_num_herbivores_increases_when_certain_birth(species, num_animals, mocker):
    """Test that population of herbivores does expand when birth is certainly occurring"""
    Herbivore.set_params({'zeta': 0.001}),  # Makes sure that weight > zeta*(w_birth + sigma_birth
    Carnivore.set_params({'zeta': 0.001})
    mocker.patch('random.random', return_value=0)

    ini_num_animals = num_animals
    ini_pop = [{'species': species, 'age': 5, 'weight': 20}
               for _ in range(ini_num_animals)]

    lowland = Lowland()
    lowland.add_population(ini_pop)
    lowland.procreation()
    num_animals_after = lowland.get_num_pr_species()[species]

    assert num_animals_after > ini_num_animals


class TestAgingWeightLossDeath:
    """
    Tests annual aging, weight_loss and death.
    """

    @pytest.fixture(autouse=True)
    def create_highland_add_population(self):
        """Create a lowland landscape and add herbivores and carnivores"""
        self.num_species = 5
        self.age, self.weight = 5, 20
        self.ini_herbs = [{'species': 'Herbivore', 'age': self.age, 'weight': self.weight}
                          for _ in range(self.num_species)]
        self.ini_carns = [{'species': 'Carnivore', 'age': self.age, 'weight': self.weight}
                          for _ in range(self.num_species)]

        self.highland = Highland()
        self.highland.add_population(animals=self.ini_herbs)
        self.highland.add_population(animals=self.ini_carns)

    @pytest.mark.parametrize('species', [Herbivore, Carnivore])
    def test_amount_calling_aging(self, mocker, species):
        """
        Tests that .aging() makes the expected amount of calls to age_update()
        """
        mocker.spy(species, 'age_update')
        self.highland.aging()
        assert species.age_update.call_count == self.num_species

    @pytest.mark.parametrize('species', ['Herbivore', 'Carnivore'])
    def test_aging_species(self, species):
        species_ages_before = self.highland.get_ages_pr_species()[species]
        self.highland.aging()
        species_ages_after = self.highland.get_ages_pr_species()[species]
        assert (np.array(species_ages_after) == np.array(species_ages_before) + 1).all()

    @pytest.mark.parametrize('species', [Herbivore, Carnivore])
    def test_amount_calling_weight_loss(self, mocker, species):
        """Tests that .weight_loss() makes the expected amount of calls to weight_loss()"""
        mocker.spy(species, 'weight_loss')
        self.highland.weight_loss()
        assert species.weight_loss.call_count == self.num_species

    @pytest.mark.parametrize('species, specie', [('Herbivore', Herbivore),
                                                 ('Carnivore', Carnivore)])
    def test_weight_species(self, species, specie):
        species_w_before = self.highland.get_weight_pr_species()[species]
        self.highland.weight_loss()
        species_w_after = self.highland.get_weight_pr_species()[species]
        weight_loss = (1 - specie.params['eta'])
        assert (np.array(species_w_after) == np.array(species_w_before) * weight_loss).all()

    @pytest.mark.parametrize('species', [Herbivore, Carnivore])
    def test_amount_calling_death(self, mocker, species):
        """Tests that .death() makes the expected amount of calls to death()"""
        mocker.spy(species, 'death')
        self.highland.death()
        assert species.death.call_count == self.num_species

    @pytest.mark.parametrize('species, specie', [('Herbivore', Herbivore),
                                                 ('Carnivore', Carnivore)])
    @pytest.mark.parametrize('omega', [0.9, 0.5, 0.3])
    def test_landscape_death(self, omega, species, specie):
        """BINOMIAL TEST

        Performs a binomial test where the probability of death is p_death.
        p_death is calculated by 'omega' * (1 - phi), and for this test the omega
        is run through 3 different values, while the fitness is set to be 1/4 by
        changing the values of parameter a_half to same as age and w_half to same as weight.
        The test returns True if the p-value of the hypothesis test is greater than
        the chosen ALPHA-significance level, set to be 5%

        """
        specie.set_params({'a_half': self.age, 'w_half': self.weight, 'omega': omega})
        # Fitness values is calculated to 0.25 when a_half = age and w_half = weight.
        p_death = omega * (1 - 0.25)
        num_animals_before = self.highland.get_num_pr_species()[species]
        self.highland.death()
        dead_animals = num_animals_before - self.highland.get_num_pr_species()[species]

        pass_a = (binom_test(dead_animals, num_animals_before, p_death) > ALPHA)

        assert pass_a


class TestMigration:

    @pytest.fixture(autouse=True)
    def create_desert_add_population(self):
        """Create a desert landscape and add herbivores and carnivores"""
        self.num_species = 3
        self.age, self.weight = 3, 18
        self.ini_herbs = [{'species': 'Herbivore', 'age': self.age, 'weight': self.weight}
                          for _ in range(self.num_species)]
        self.ini_carns = [{'species': 'Carnivore', 'age': self.age, 'weight': self.weight}
                          for _ in range(self.num_species)]

        self.desert = Desert()
        self.desert.add_population(animals=self.ini_herbs)
        self.desert.add_population(animals=self.ini_carns)

    def test_no_emigration_when_migrated_before(self, mocker):
        """Tests that animals that have already migrated,
        will not be added to emigrating animals from current location"""
        mocker.patch('random.random', return_value=0)  # Makes animal move with certainty

        for animal in itertools.chain(self.desert.herbivores, self.desert.carnivores):
            animal.migrated = True
        emigrating_herbivores, emigrating_carnivores = self.desert.emigration()
        assert len(emigrating_herbivores) == 0 and len(emigrating_carnivores) == 0

    @pytest.mark.parametrize('emi_pop', [0, 1])
    def test_all_emigrate_when_both_conditions_met(self, mocker, emi_pop):
        """Tests that all the animals that have not migrated,
        will all be added to emigrating animals when emigration is certainly happening"""
        mocker.patch('random.random', return_value=0)  # Makes animal move with certainty

        emigrating_pop = self.desert.emigration()[emi_pop]

        assert len(emigrating_pop) == self.num_species

    @pytest.mark.parametrize('species, specie', [('Herbivore', Herbivore),
                                                 ('Carnivore', Carnivore)])
    def test_add_animal_immigrated_to_location(self, species, specie):
        """Tests that all the immigrating animals to current location,
        will be added to their respective species population"""
        immigrated_animal = specie(ini_age=5, ini_weight=20)
        pop_before = self.desert.get_num_pr_species()[species]
        self.desert.add_immigrated_animal(immigrated_animal)
        pop_after = self.desert.get_num_pr_species()[species]

        assert pop_after == pop_before + 1 and immigrated_animal.migrated is True

    def test_reset_migration(self):
        """Tests that all the animals have their migration-status reset to False from True"""
        for animal in itertools.chain(self.desert.herbivores, self.desert.carnivores):
            animal.migrated = True
        self.desert.reset_migration()
        herbi_reset_move = [herbi.migrated for herbi in self.desert.herbivores]
        carni_reset_move = [carni.migrated for carni in self.desert.carnivores]

        assert all(np.array(herbi_reset_move)) is False and all(np.array(carni_reset_move)) is False
